import os
import re
import sys
import subprocess
import time


def user_query(question, variants):
	r = ''
	for v in variants:
		if r != '':
			r += '/'
		if r == '':
			v = v.upper()
		r += v

	while True:
		v = input('%s [%s]: ' % (question, r))
		if v == '':
			return variants[0]
		for fv in variants:
			if v.lower() == fv:
				return fv
		print('Необходимо ввести одно из следующих значений ', variants)


def execute_cmd(cmd):
	process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
	process.wait()

	if process.returncode != 0:
		raise Exception("Команда %s выполнилась с ошибкой %d"%(str(cmd), process.returncode))

	r = []
	for line in process.stdout.readlines():
		s = line.decode(encoding="cp1251").strip()
		if s != '':
			r.append(s)
	return r


def get_repo_time(repo_path) -> time.struct_time:
	cur = os.curdir
	try:
		#print("Папка репозитория:", repo_path)
		os.chdir(repo_path)
		cmd = ['hg', 'heads']
		out_lines = execute_cmd(cmd)
		#print(out_lines)
		out_len = len(out_lines)
		if out_len < 5:
			raise Exception("Должно быть не меньше 5 агрументов! " + str(out_len))

		time_out_line = out_lines[len(out_lines) - 2]
		r = re.compile("\w{3} \d{1,2} \d{1,2}:\d{2}:\d{2} \d{4}", re.IGNORECASE)
		time_str = r.search(time_out_line)
		if time_str is None:
			raise Exception("Невозможно извлечь время! " + time_out_line)
		#print(time_str.group())
		r = time.strptime(time_str.group(), "%b %d %H:%M:%S %Y")
		#print(r)
		return r
	except Exception as e:
		raise Exception("get_repo_time '%s' >> %s"%(repo_path, str(e)))
	finally:
		os.chdir(cur)


def is_repo_commit(repo_path) -> bool:
	cur = os.curdir
	try:
		os.chdir(repo_path)
		cmd = ["hg", "status", "--subrepos"]
		out_lines = execute_cmd(cmd)
		if len(out_lines) == 0:
			return True

		print("Имеются незафиксированные изменения:")
		for line in out_lines:
			print("\t" + line)

		return  False
	except Exception as e:
		raise Exception("is_repo_commit '%s' >> %s"%(repo_path, str(e)))
	finally:
		os.chdir(cur)


def get_parent_path() -> str:
	try:
		cmd_out = execute_cmd(['hg', 'paths'])
		if len(cmd_out) != 1:
			raise Exception("Родительских папок слишком много!\n%s"%cmd_out)
		reg = re.compile("[^=]*$", re.IGNORECASE)
		r = reg.search(cmd_out[0])
		if r is None:
			raise Exception("Невозможно извлечь путь из строки '%s'"%cmd_out[0])
		return r.group().strip()
	except Exception as e:
		raise Exception("Извлечение родительской папки >> " + str(e))


def repo_push():
	r = user_query("Протолкнуть изменения?", ["y", "n"])
	if r != "y":
		raise UserWarning
	out_lines = execute_cmd(['hg', '-v', 'push'])
	for line in out_lines:
			print("\t" + line)


def repo_commit():
	r = user_query("Провести фиксацию?", ["y", "n"])
	if r != "y":
		raise UserWarning
	v = input('Описание: ')
	if len(v) == 0:
		v = "Sync.py"
	out_lines = execute_cmd(['hg', '-v', 'commit', '-A', "-m%s"%v])
	for line in out_lines:
			print("\t" + line)


def repo_pull():
	print("Обновление локального репозитория из корня")
	out_lines = execute_cmd(['hg', '-v', 'pull', '-u'])
	for line in out_lines:
			print("\t" + line)


try:
	TIME_FORMAT = "%Y.%m.%d %H:%M:%S"
	local_path = os.path.abspath(os.curdir)
	os.system('cls')

	local_repo_time = get_repo_time(local_path)
	print('Последнее изменение в локальном репозитории:\n\t', time.strftime(TIME_FORMAT, local_repo_time))
	parent_path = get_parent_path()
	print('Папка родительского репозитория:\n\t', parent_path)
	parent_repo_time = get_repo_time(parent_path)
	print('Последнее изменение в родительском репозитории:\n\t', time.strftime(TIME_FORMAT, parent_repo_time))

	is_commit = is_repo_commit(local_path)

	proc = False
	# Хороший конец хорошего начала.
	if (not is_commit) and (local_repo_time == parent_repo_time):
		print("Хороший конец хорошего начала")
		repo_commit()
		repo_push()
		proc = True

	if is_commit and (local_repo_time == parent_repo_time):
		print("Изменений нет. Ничего делать не нужно.")
		proc = True

	# Была внешнаяя фиксация.
	if is_commit and (local_repo_time > parent_repo_time):
		print("Была внешнаяя фиксация.")
		repo_push()
		proc = True

	if is_commit and (local_repo_time < parent_repo_time):
		repo_pull()
		proc = True

	if not proc:
		raise Exception("Нетипичный случай")
	input("Для завершения программы нажмите <Enter>")
except UserWarning as e:
	print('Прервано пользователем!')
	exit(1)
except Exception as e:
	print('Критическая ошибка >>', str(e))
	input("Для завершения программы нажмите <Enter>")
	exit(2)
